module gitlab.com/adivinagame/backend/maxadivinabackend/api-contacto

go 1.12

require (
	github.com/labstack/echo/v4 v4.1.10
	gitlab.com/adivinagame/backend/maxadivinabackend v0.0.0-20190915203932-54827be54fd4
	gitlab.com/adivinagame/backend/maxadivinabackend/api-palabras v0.0.0-20190915203932-54827be54fd4 // indirect
	golang.org/x/tools v0.0.0-20190915201606-1d8cfc4bd2ca // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.2.2
)
