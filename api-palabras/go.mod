module gitlab.com/adivinagame/backend/maxadivinabackend/api-palabras

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-playground/locales v0.13.0 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/valyala/fasttemplate v1.1.0 // indirect
	gitlab.com/adivinagame/backend/maxadivinabackend v0.0.0-20191106023932-09404cc26c1a
	golang.org/x/crypto v0.0.0-20191105034135-c7e5f84aec59 // indirect
	gopkg.in/go-playground/validator.v9 v9.30.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.2.5
)
